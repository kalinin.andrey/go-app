module redditclone

go 1.13

require (
	github.com/DATA-DOG/go-sqlmock v1.5.0
	github.com/alicebob/gopher-json v0.0.0-20200520072559-a9ecdc9d1d3a // indirect
	github.com/alicebob/miniredis v2.5.0+incompatible
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/elliotchance/redismock v1.5.3 // indirect
	github.com/elliotchance/redismock/v8 v8.3.3
	github.com/go-ozzo/ozzo-routing/v2 v2.3.0
	github.com/go-ozzo/ozzo-validation v3.6.0+incompatible // indirect
	github.com/go-ozzo/ozzo-validation/v4 v4.2.2
	github.com/go-redis/cache/v8 v8.2.1
	github.com/go-redis/redis v6.15.9+incompatible // indirect
	github.com/go-redis/redis/v8 v8.3.3
	github.com/golang/protobuf v1.4.3
	github.com/google/uuid v1.1.1
	github.com/heetch/confita v0.9.0
	github.com/iancoleman/strcase v0.1.2
	github.com/jinzhu/copier v0.0.0-20190924061706-b57f9002281a
	github.com/jinzhu/gorm v1.9.16
	github.com/mattn/go-sqlite3 v2.0.1+incompatible // indirect
	github.com/pkg/errors v0.9.1
	github.com/spf13/cobra v1.1.1
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.6.1
	github.com/yuin/gopher-lua v0.0.0-20200816102855-ee81675732da // indirect
	go.mongodb.org/mongo-driver v1.4.3
	go.uber.org/zap v1.16.0
	golang.org/x/crypto v0.0.0-20201016220609-9e8e0b390897
	google.golang.org/protobuf v1.25.0
	gopkg.in/asaskevich/govalidator.v9 v9.0.0-20180315120708-ccb8e960c48f // indirect
)
